/*
 * ip_functions.h
 *
 *  Created on: Feb 23, 2018
 *      Author: the Legend
 */

#ifndef IP_FUNCTIONS_H_
#define IP_FUNCTIONS_H_


/* ---------------------------------------------------------------------------- *
 * 								Header Files									*
 * ---------------------------------------------------------------------------- */
#include <stdio.h>
#include <sleep.h>
#include "xiicps.h"
#include <xil_printf.h>
#include <xparameters.h>
#include "xgpio.h"
#include "xuartps.h"
#include "stdlib.h"
#include "xadcps.h"
#include "xbram.h"
#include "xtmrctr_l.h"
#include "xtmrctr.h"
#include "xscugic.h"
/* ---------------------------------------------------------------------------- *
 * 							Custom IP Header Files								*
 * ---------------------------------------------------------------------------- */
#include "audio.h"
#include "lms_pcore_addr.h"
#include "xnco.h"

/* ---------------------------------------------------------------------------- *
 * 							Prototype Functions									*
 * ---------------------------------------------------------------------------- */
void menu();
void tonal_noise();
void audio_stream();
void lms_filter();
unsigned char gpio_init();
void nco_init(void *InstancePtr);
int XAdcFractionToInt(float FloatNum);
void XAdcInit(void);
void bram_init(void);
void GPIOCustomSetup(void);
void TimerSetup(void);
void zybo_button_scan(u32 buttonRead);
void zybo_switch_scan(u32 switchRead);
/* ---------------------------------------------------------------------------- *
 * 						Redefinitions from xparameters.h 						*
 * ---------------------------------------------------------------------------- */
#define NCO_ID XPAR_NCO_0_DEVICE_ID

#define LMS_LOC XPAR_LMS_PCORE_0_BASEADDR
#define LMS_X LMS_LOC + x_k__Data_lms_pcore
#define LMS_D LMS_LOC + d_k__Data_lms_pcore
#define LMS_E LMS_LOC + e_k__Data_lms_pcore
#define LMS_STROBE LMS_LOC + IPCore_Strobe_lms_pcore

#define UART_BASEADDR XPAR_PS7_UART_1_BASEADDR

#define BUTTON_SWITCH_BASE XPAR_GPIO_1_BASEADDR
#define LED_BASE XPAR_LED_CONTROLLER_0_S00_AXI_BASEADDR
#define BUTTON_SWITCH_ID XPAR_GPIO_1_DEVICE_ID
#define AUDIO_ENABLE_ID XPAR_AXI_GPIO_0_DEVICE_ID
#define XADC_DEVICE_ID 		XPAR_XADCPS_0_DEVICE_ID
#define BRAM_DEVICE_ID		XPAR_BRAM_0_DEVICE_ID
#define GPIOCUSTOM_DEVICE_ID	XPAR_GPIO_2_DEVICE_ID
#define TMR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID

/* ---------------------------------------------------------------------------- *
 * 							Define GPIO Channels								*
 * ---------------------------------------------------------------------------- */
#define BUTTON_CHANNEL 1
#define SWITCH_CHANNEL 2

/* ---------------------------------------------------------------------------- *
 * 							Audio Scaling Factor								*
 * ---------------------------------------------------------------------------- */
#define SCALE 7


/* ---------------------------------------------------------------------------- *
 * 							SEANS MACROS												*
 * ---------------------------------------------------------------------------- */
#define TMR_LOAD 0xFF100000
#define LED_ONE	0x07
#define LED_TWO	0x08

/* ---------------------------------------------------------------------------- *
 * 							Global Variables									*
 * ---------------------------------------------------------------------------- */
XIicPs Iic;
XGpio Gpio; // Gpio instance for buttons and switches
XGpio Gpio_audio_enable; // GPIO instance for digital mute
XNco Nco;
XBram Bram;
XAdcPs XADCMonInst;
XBram_Config *ConfigPtr;
XGpio GPIOCustomInst;
XTmrCtr	TMRInst;
#endif /* IP_FUNCTIONS_H_ */
