/*
 * ip_functions.c
 *
 * Contains all functions which pertain to setup and use of IP periperals.
 */

#include "ip_functions.h"
#include "audio.h"
#include <stdlib.h>
#include "sounds.h"



static void SetupInterruptSystem(XScuGic *GicInstancePtr, XTtcPs *TtcPsInt)
{


		XScuGic_Config *IntcConfig; //GIC config
		Xil_ExceptionInit();

		//initialise the GIC
		IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);

		XScuGic_CfgInitialize(GicInstancePtr, IntcConfig,
						IntcConfig->CpuBaseAddress);

	    //connect to the hardware
		Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
					(Xil_ExceptionHandler)XScuGic_InterruptHandler,
					GicInstancePtr);

		XScuGic_Connect(GicInstancePtr, TTC_INTR_ID,
				(Xil_ExceptionHandler)TickHandler, (void *)TtcPsInt);


		XScuGic_Enable(GicInstancePtr, TTC_INTR_ID);
		XTtcPs_EnableInterrupts(TtcPsInt, XTTCPS_IXR_INTERVAL_MASK);

		XTtcPs_Start(TtcPsInt);

		// Enable interrupts in the Processor.
		Xil_ExceptionEnableMask(XIL_EXCEPTION_IRQ);
	}

static void TickHandler(void *CallBackRef)
{
	u32 StatusEvent;
	u32 step;
	StatusEvent = XTtcPs_GetInterruptStatus((XTtcPs *)CallBackRef);
	XTtcPs_ClearInterruptStatus((XTtcPs *)CallBackRef, StatusEvent);

	step = 	Xil_In32(LED_BASE);
	Xil_Out32(LED_BASE,!(step));
	printf("event\n\r");

}


void setupTimer(void)
{
	TimerSetup = &SettingsTable[TTC_DEVICE_ID];

		//Timer = &(TtcPsInst[TTC_DEVICE_ID]);
		XTtcPs_Stop(&Timer);


	 	//initialise the timer
	 	Config = XTtcPs_LookupConfig(TTC_DEVICE_ID);
	 	XTtcPs_CfgInitialize(&Timer, Config, Config->BaseAddress);

	 	TimerSetup->Options |= (XTTCPS_OPTION_INTERVAL_MODE |
	 			XTTCPS_OPTION_MATCH_MODE);
	    static u32 MatchValue = 0;

	 	XTtcPs_SetOptions(&Timer, TimerSetup->Options);
	 	XTtcPs_CalcIntervalFromFreq(&Timer, TimerSetup->OutputHz,&(TimerSetup->Interval), &(TimerSetup->Prescaler));

	    XTtcPs_SetInterval(&Timer, TimerSetup->Interval);
	    XTtcPs_SetPrescaler(&Timer, TimerSetup->Prescaler);

	    SetupInterruptSystem(&Intc, &Timer);
}

	void bram_init(void)
	{
		int Status;
		/*
			 * Initialize the BRAM driver. If an error occurs then exit
			 */

			/*
			 * Lookup configuration data in the device configuration table.
			 * Use this configuration info down below when initializing this
			 * driver.
			 */
			ConfigPtr = XBram_LookupConfig(XPAR_BRAM_0_DEVICE_ID);
			if (ConfigPtr == (XBram_Config *) NULL) {
			printf("Bram init failed\r\n");
			}

			Status = XBram_CfgInitialize(&Bram, ConfigPtr,
						     ConfigPtr->CtrlBaseAddress);
			if (Status != XST_SUCCESS) {
				printf("Bram init failed\r\n");
			}


			/*
			 * Execute the BRAM driver selftest.
			 */
			Status = XBram_SelfTest(&Bram, 0);
			if (Status != XST_SUCCESS) {
				printf("Bram init failed\r\n");
			}
	}
	/* ---------------------------------------------------------------------------- *
	 * 								ADC CONVERSION*
	 * ---------------------------------------------------------------------------- *	*/
int XAdcFractionToInt(float FloatNum)
{
	float Temp;
	Temp = FloatNum;
	if (FloatNum < 0) {
	Temp = -(FloatNum);
		}
	return( ((int)((Temp -(float)((int)Temp)) * (1000.0f))));
}
	void XAdcInit(void)
	{
		XAdcPs_Config *ConfigPtr;
			XAdcPs *XADCInstPtr = &XADCMonInst;
			int Status_ADC;
			 ConfigPtr = XAdcPs_LookupConfig(XADC_DEVICE_ID);
			       	if (ConfigPtr == NULL) {
				           print("ADC INIT FAILED\n\r");
			     	}

			       Status_ADC = XAdcPs_CfgInitialize(XADCInstPtr,ConfigPtr,ConfigPtr->BaseAddress);
			       if(XST_SUCCESS != Status_ADC){
			           print("ADC INIT FAILED\n\r");
			        }

			       //self test
			       Status_ADC = XAdcPs_SelfTest(XADCInstPtr);
			   	if (Status_ADC != XST_SUCCESS) {
			           print("ADC INIT FAILED\n\r");
			   	}

			   	//stop sequencer
			   	XAdcPs_SetSequencerMode(XADCInstPtr,XADCPS_SEQ_MODE_SINGCHAN);

			       //disable alarms
			       XAdcPs_SetAlarmEnables(XADCInstPtr, 0x0);

			       //configure sequencer to just sample internal on chip parameters
			       XAdcPs_SetSeqInputMode(XADCInstPtr, XADCPS_SEQ_MODE_SINGCHAN);

			       //configure the channel enables we want to monitor
			      // XAdcPs_SetSeqChEnables(XADCInstPtr,30);
			       XAdcPs_SetSingleChParams(XADCInstPtr,30,1,0,0);
			       XAdcPs_SetAvg(XADCInstPtr,XADCPS_AVG_64_SAMPLES);
	}

/* ---------------------------------------------------------------------------- *
 * 								lms_filter()									*
 * ---------------------------------------------------------------------------- *
 * This function adds a tonal noise component to the sampled audio from the
 * audio codec by passing a step size to the input of an NCO component in the
 * PL. A sinusoidal signal is received back from the NCO which is then scaled
 * and added to the sampled audio. The audio + noise sample can then be
 * adaptively filtered using an LMS filter in the PL. The resulting audio,
 * filtered or not, is then output to the audio codec.
 *
 * The main menu can be accessed by entering 'q' on the keyboard.
 * ---------------------------------------------------------------------------- */
void lms_filter()
{
	u32 nco_in, nco_out, in_left, in_right, out_left, out_right, step, \
		prevL, prevR, prevTone, temp;

	/* Read step size value from DIP switches */
	step = XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL);

	/* Write step size value to the LEDs */
	Xil_Out32(LED_BASE, step);

	/* Scale the step size */
	nco_in = step;
	xil_printf("Step = %d, nco_in = %d\r\n",step, nco_in);

	while (!XUartPs_IsReceiveData(UART_BASEADDR)){

		/* Input step size to the NCO core */
		XNco_Set_step_size_V(&Nco, nco_in);

		/* Receive sinusoidal sample from NCO core */
		nco_out = XNco_Get_sine_sample_V(&Nco);

		if(nco_out!=prevTone) { /* New sinusoidal sample? */
			temp = nco_out;
		}

		/* Sample L+R audio from the codec */

		in_left = Xil_In32(I2S_DATA_RX_L_REG);
		in_right = Xil_In32(I2S_DATA_RX_R_REG);

		/* -------------------------------------------------------------------------------- *
		 * --------------------------------- LEFT CHANNEL --------------------------------- *
		 * -------------------------------------------------------------------------------- */
		if(in_left != prevL) /* New left sample? */
		{
			/* Add noise component to the L+R audio samples */
			out_left = (temp + in_left);

			Xil_Out32(LMS_D, out_left >> SCALE);	// Input audio+noise as desired signal
			Xil_Out32(LMS_X, temp >> SCALE);		// Input noise as input
			Xil_Out32(LMS_STROBE, 0x01);			// Stobe LMS to signal inputs are finished

			/* If any button is pressed */
			if(XGpio_DiscreteRead(&Gpio, BUTTON_CHANNEL)>0){

				/* Wait until output data is ready */
				out_left = (Xil_In32(LMS_E) << (SCALE-1)); // Output filtered audio
			}

			/* Output audio to the codec */
			Xil_Out32(I2S_DATA_TX_L_REG, out_left);

		}

		/* -------------------------------------------------------------------------------- *
		 * --------------------------------- RIGHT CHANNEL -------------------------------- *
		 * -------------------------------------------------------------------------------- */
		if(in_right != prevR) /* New right sample? */
		{
			/* Add scaled noise component to the L+R audio samples */
			out_right = (temp + in_right);

			Xil_Out32(LMS_D, out_right >> SCALE); 	// Input audio+noise as desired signal
			Xil_Out32(LMS_X, temp >> SCALE); 		// Input noise as input
			Xil_Out32(LMS_STROBE, 0x01);			// Stobe LMS to signal inputs are finished

			/* If any button is pressed */
			if(XGpio_DiscreteRead(&Gpio, BUTTON_CHANNEL)>0){
				out_right = (Xil_In32(LMS_E) << (SCALE-1)); // output filtered audio
			}

			/* Output audio to the codec */
			Xil_Out32(I2S_DATA_TX_R_REG, out_right);
		}

		/* Update previous input values */
		prevL = in_left;
		prevR = in_right;
		prevTone = nco_out;

		/* If the DIP switch values have changed, break from while
		 * loop to allow the step size value to update.
		 */
		if(step != XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL)) break;
	} // while
	/* If input from the terminal is 'q', then return to menu.
	 * Else, continue. */
	if(XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET)=='q') menu();
	else lms_filter();

} // LMS filtering

/* ---------------------------------------------------------------------------- *
 * 								tonal_noise()									*
 * ---------------------------------------------------------------------------- *
 * This function adds a tonal noise component to the sampled audio from the
 * audio codec by passing a step size to the input of an NCO component in the
 * PL. A sinusoidal signal is received back from the NCO which is then scaled
 * and added to the sampled audio. The audio + noise sample is then sent to
 * the audio codec for output.
 *
 * The main menu can be accessed by entering 'q' on the keyboard.
 * ---------------------------------------------------------------------------- */
void tonal_noise(void)
{
	u32 nco_in, nco_out, in_left, in_right, out_left, out_right, step, temp;

	/* Read step size value from DIP switches */
	step = XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL);

	/* Write step size value to the LEDs */
	Xil_Out32(LED_BASE, step);

	/* Scale the step size */
	nco_in = step;

	xil_printf("Step = %d, nco_in = %d\r\n",step, nco_in);

	while (!XUartPs_IsReceiveData(UART_BASEADDR)){

		/* Input scaled step size to the NCO core */
		XNco_Set_step_size_V(&Nco, nco_in);

		/* Receive sinusoidal sample from NCO core */
		nco_out = XNco_Get_sine_sample_V(&Nco);

		temp = nco_out;

		/* Sample L+R audio from the codec */
		in_left = Xil_In32(I2S_DATA_RX_L_REG);
		in_right = Xil_In32(I2S_DATA_RX_R_REG);


		/* Add scaled noise component to the L+R audio samples */
		out_left =  temp + in_left;
		out_right = temp + in_right;

		/* Output corrupted audio to the codec */
		Xil_Out32(I2S_DATA_TX_L_REG, out_left);
		Xil_Out32(I2S_DATA_TX_R_REG, out_right);

		/* If the DIP switch values have changed, break from while
		 * loop to allow the step size value to update.
		 */
		if(step != XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL)) break;
	} // while
	/* If input from the terminal is 'q', then return to menu.
	 * Else, continue. */
	if(XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET) == 'q') menu();
	else tonal_noise();

} // tonal_noise()

/* ---------------------------------------------------------------------------- *
 * 								audio_stream()									*
 * ---------------------------------------------------------------------------- *
 * This function performs audio loopback streaming by sampling the input audio
 * from the codec and then immediately passing the sample to the output of the
 * codec.
 *
 * The main menu can be accessed by entering 'q' on the keyboard.//
 * ---------------------------------------------------------------------------- */
void audio_stream(){

//volume = 0xFFFFFFFF; 	//XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL);
//	AudioWriteToReg(R3_RIGHT_CHANNEL_DAC_VOLUME, 		(u16)Volume.vol);

	//int index = 0;
	s32 delay_left,delay_right;
	s32 delay_left_1,delay_right_1;
	u32  in_left, in_right;
	s32 in_left_signed,in_right_signed ;
	s32 processed_left, processed_right;
	u32 VccPintRawData,step;
	volatile	float VccPintData;

//	step = XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL);

	while (!XUartPs_IsReceiveData(UART_BASEADDR)){
	//	AudioWriteToReg(R2_LEFT_CHANNEL_DAC_VOLUME, 		volume);
		VccPintRawData = XAdcPs_GetAdcData(&XADCMonInst, 30);
					VccPintData = (float)(VccPintRawData)/65536.0f;

		step = XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL);

		/* Write step size value to the LEDs */
		Xil_Out32(LED_BASE, step);

		// Read audio input from codec
		in_left = Xil_In32(I2S_DATA_RX_L_REG);
		in_right = Xil_In32(I2S_DATA_RX_R_REG);


		in_left_signed = (in_left<<8); // move sign bit to MSB of u32
		in_right_signed = (in_right<<8);

//		in_left_signed = in_left_signed*(((step<<27)|0x07FFFFFF)/fullVolume); scale volume to buttons
	//	in_right_signed = in_right_signed*(((step<<27)|0x07FFFFF)/fullVolume);


	//	in_left_signed = in_left_signed*(((step<<27)|0x07FFFFFF)*constantVolume); //Mask bottomn bits of step
	//	in_right_signed = in_right_signed*(((step<<27)|0x07FFFFF)*constantVolume);
		in_left_signed = ((float)(in_left_signed) *VccPintData);
		in_right_signed = ((float)(in_right_signed)*VccPintData);


		if (step & 0x01)
		{
		processed_left = (LPF((float)in_left_signed,(float)delay_left,(float)delay_left_1));
		processed_right = (LPF((float)in_right_signed,(float)delay_right,(float)delay_right_1));
		}
		else if (step & 0x02)
		{
			processed_left = (HPF((float)in_left_signed,(float)delay_left,delay_right_1));
			processed_right = (HPF((float)in_right_signed,(float)delay_right,delay_right_1));
		}
		else
		{
			processed_left = in_left_signed;
			processed_right = in_right_signed;
		}

		in_left = (processed_left )>>8;
		in_right = (processed_right)>>8;


//		in_left = (in_left_signed + delay_left)>>8;
	//	in_right = (in_right_signed + delay_right)>>8;

			// Write audio input to codec
		Xil_Out32(I2S_DATA_TX_L_REG, in_left);
		Xil_Out32(I2S_DATA_TX_R_REG, in_right);



		delay_left_1 = delay_left;
		delay_right_1 = delay_right;

		delay_left = (float)in_left_signed;
		delay_right = (float)in_right_signed;


		//if(step != XGpio_DiscreteRead(&Gpio, SWITCH_CHANNEL)) break;
	//	if (VccPintRawData != XAdcPs_GetAdcData(&XADCMonInst, 30)) break;
	}
	/* If input from the terminal is 'q', then return to menu.
	 * Else, continue streaming. */

	if(XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET) == 'q') menu();

	else audio_stream();
} // audio_stream()


/* ---------------------------------------------------------------------------- *
 * 								gpio_initi()									*
 * ---------------------------------------------------------------------------- *
 * Initialises the GPIO driver for the push buttons and switches.
 * ---------------------------------------------------------------------------- */
unsigned char gpio_init()
{
	int Status;

	Status = XGpio_Initialize(&Gpio, BUTTON_SWITCH_ID);
	if(Status != XST_SUCCESS) return XST_FAILURE;
	Status = XGpio_Initialize(&Gpio_audio_enable, AUDIO_ENABLE_ID);
	if(Status != XST_SUCCESS) return XST_FAILURE;

	XGpio_SetDataDirection(&Gpio_audio_enable, 1, 0x00);
	XGpio_SetDataDirection(&Gpio, SWITCH_CHANNEL, 0xFF);
	XGpio_SetDataDirection(&Gpio, BUTTON_CHANNEL, 0xFF);

	return XST_SUCCESS;
}

/* ---------------------------------------------------------------------------- *
 * 								nco_initi()									*
 * ---------------------------------------------------------------------------- *
 * Initialises the NCO driver by looking up the configuration in the config
 * table and then initialising it.
 * ---------------------------------------------------------------------------- */
void nco_init(void *InstancePtr){
	XNco_Config *cfgPtr;
	int status;

	/* Initialise the NCO driver so that it's ready to use */

	// Look up the configuration in the config table
	cfgPtr = XNco_LookupConfig(NCO_ID);
	if (!cfgPtr) {
		print("ERROR: Lookup of NCO configuration failed.\n\r");
	}

	// Initialise the NCO driver configuration
	status = XNco_CfgInitialize(InstancePtr, cfgPtr);
	if (status != XST_SUCCESS) {
		print("ERROR: Could not initialise NCO.\n\r");
	}
}
