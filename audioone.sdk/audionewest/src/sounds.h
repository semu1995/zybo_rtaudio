/*
 * sounds.h
 *
 *  Created on: Feb 23, 2018
 *      Author: the Legend
 */

#ifndef SOUNDS_H_
#define SOUNDS_H_
#include <stdio.h>
#include <xil_io.h>

float LPF(float sample, float del,float del1);
float HPF(float sample, float del,float del1);
#endif /* SOUNDS_H_ */
